package tech.functionalflame

import mu.KotlinLogging
import java.io.*
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry as InternalZipEntry

private val log = KotlinLogging.logger {}

@Throws(IOException::class)
fun zipTo(entries: Iterable<ZipEntry>, output: OutputStream) =
  ZipOutputStream(output).use { zip ->
    entries.forEach { (input, filename) ->
      zip.addEntry(InternalZipEntry(filename), input())
    }
  }

private fun ZipOutputStream.addEntry(entry: InternalZipEntry, input: InputStream) {
  putNextEntry(entry)
  input.use { it.pipe(this) }
  closeEntry()
  log.info("Entry added: {}", entry.name)
}

data class ZipEntry(val input: () -> InputStream, val filename: String)