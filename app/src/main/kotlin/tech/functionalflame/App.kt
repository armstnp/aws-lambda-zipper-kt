package tech.functionalflame

import com.amazonaws.ClientConfiguration
import com.amazonaws.regions.Regions
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.internal.Constants
import tech.functionalflame.S3MultipartUpload.IsFinalPart

// N.B. Part Size should not be less than 5 MB while using MultipartUpload
private const val UPLOAD_PART_SIZE = 5 * Constants.MB

private val TEST_ZIP_INPUT =
  ZipInput(sources = listOf(), output = Output("", ""))

class Source(var bucket: String = "", var key: String = "", var filename: String = "") {
  operator fun component1() = bucket
  operator fun component2() = key
  operator fun component3() = filename
}
class Output(var bucket: String = "", var key: String = "") {
  operator fun component1() = bucket
  operator fun component2() = key
}
class ZipInput(var sources: Iterable<Source> = listOf(), var output: Output = Output()) {
  operator fun component1() = sources
  operator fun component2() = output
}

class App : RequestHandler<ZipInput, Map<String, Boolean>> {
  override fun handleRequest(input: ZipInput?, context: Context?): Map<String, Boolean> {
    checkNotNull(input)

    runHandler(input)

    return mapOf("success" to true)
  }

  fun main() {
    runHandler(TEST_ZIP_INPUT)
  }
}

fun runHandler(input: ZipInput) {
  val s3Client = AmazonS3ClientBuilder
    .standard()
    .withRegion(Regions.US_WEST_2)
    .withClientConfiguration(ClientConfiguration().withTcpKeepAlive(true))
    .build()

  zipViaS3(s3Client, input)
}

private fun zipViaS3(s3Client: AmazonS3, zipInput: ZipInput) {
  val (sources, out) = zipInput
  val (outBucket, outKey) = out

  val s3Streamer = S3Streamer(s3Client)
  val multipartUpload =
    S3MultipartUpload(s3Client, outBucket, outKey, contentType = "application/zip")
      .also { it.initializeUpload() }

  val sourceStreams = sources.map { (bucket, key, filename) ->
    ZipEntry(
      { s3Streamer.stream(bucket, key) },
      filename)
  }

  threadPipe(
    { output -> zipTo(sourceStreams, output) },
    { input ->
      StreamChunker(UPLOAD_PART_SIZE, input)
        .drain { multipartUpload.uploadPart(it, IsFinalPart.Normal) }
        .finally { multipartUpload.uploadPart(it, IsFinalPart.Final) }
    })
}
