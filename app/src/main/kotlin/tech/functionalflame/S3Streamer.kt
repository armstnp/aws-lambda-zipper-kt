package tech.functionalflame

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.GetObjectRequest
import java.io.InputStream

class S3Streamer(private val s3Client: AmazonS3) {
  fun stream(bucket: String, key: String): InputStream =
    s3Client
      .getObject(GetObjectRequest(bucket, key))
      .objectContent
}