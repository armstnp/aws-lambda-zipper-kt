package tech.functionalflame

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.*
import mu.KotlinLogging
import java.io.ByteArrayInputStream
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicInteger

/**
 * Code adapted from Pramesh Bhattarai
 *
 * Class for saving [ByteArrayInputStream] to S3 Bucket without Content-Length using Multipart Upload.
 *
 * Links:
 * - [Multipart Upload Overview](https://docs.aws.amazon.com/AmazonS3/latest/dev/mpuoverview.html)
 * - [Multipart Limits](https://docs.aws.amazon.com/AmazonS3/latest/dev/qfacts.html)
 * - [Aborting Multipart Upload](https://docs.aws.amazon.com/AmazonS3/latest/dev/llJavaUploadFile.html)
 */
class S3MultipartUpload(
  private val s3Client: AmazonS3,
  private val bucket: String?,
  private val key: String?,
  private val contentType: String?
) {
  private val log = KotlinLogging.logger {}
  private val executorService = Executors.newFixedThreadPool(DEFAULT_THREAD_COUNT) as ThreadPoolExecutor
  private var uploadId: String? = null

  // uploadPartId should be between 1 and 10000 inclusively
  private val uploadPartId = AtomicInteger(0)
  private val futuresPartETags = mutableListOf<Future<PartETag>>()

  private val freshObjectMetadata: ObjectMetadata
    get() = ObjectMetadata().apply { contentType = this@S3MultipartUpload.contentType }

  enum class IsFinalPart { Normal, Final }

  private fun UploadPartRequest.withLastPart(isFinalPart: IsFinalPart): UploadPartRequest =
    when (isFinalPart) {
      IsFinalPart.Normal -> this
      IsFinalPart.Final -> withLastPart(true)
    }

  /**
   * We need to call initialize upload method before calling any upload part.
   */
  fun initializeUpload() {
    val initRequest = InitiateMultipartUploadRequest(bucket, key).apply {
      objectMetadata = freshObjectMetadata // if we want to set object metadata in S3 bucket
      tagging = EMPTY_OBJECT_TAGGING // if we want to set object tags in S3 bucket
    }
    uploadId = s3Client.initiateMultipartUpload(initRequest).uploadId
  }

  fun uploadPart(inputStream: ByteArrayInputStream, isFinalPart: IsFinalPart) = when (isFinalPart) {
    IsFinalPart.Normal -> submitTaskForUploading(inputStream, isFinalPart)
    IsFinalPart.Final -> uploadFinalPartAsync(inputStream)
  }

  private fun submitTaskForUploading(inputStream: ByteArrayInputStream, isFinalPart: IsFinalPart) {
    check(!(uploadId.isNullOrEmpty())) { "Initial Multipart Upload Request has not been set." }
    check(!bucket.isNullOrEmpty()) { "Destination bucket has not been set." }
    check(!key.isNullOrEmpty()) { "Uploading file name has not been set." }

    submitTaskToExecutorService {
      try {
        val eachPartId = uploadPartId.incrementAndGet()
        val uploadRequest = UploadPartRequest()
          .withBucketName(bucket)
          .withKey(key)
          .withUploadId(uploadId)
          .withPartNumber(eachPartId) // partNumber should be between 1 and 10000 inclusively
          .withPartSize(inputStream.available().toLong())
          .withInputStream(inputStream)
          .withLastPart(isFinalPart)

        log.info("Submitting uploadPartId: {} of partSize: {}", eachPartId, inputStream.available())

        s3Client.uploadPart(uploadRequest).also {
          log.info("Successfully submitted uploadPartId: {}", eachPartId)
        }.partETag
      } catch (e: ExecutionException) {
        e.printStackTrace()
        abortUpload()
        throw RuntimeException(e)
      } catch (e: InterruptedException) {
        e.printStackTrace()
        abortUpload()
        throw RuntimeException(e)
      } catch (e: AmazonS3Exception) {
        e.printStackTrace()
        abortUpload()
        throw RuntimeException(e)
      }
    }
  }

  private fun uploadFinalPartAsync(inputStream: ByteArrayInputStream) {
    try {
      submitTaskForUploading(inputStream, IsFinalPart.Final)

      // wait and get all PartETags from ExecutorService
      // and submit it in CompleteMultipartUploadRequest
      val partETags = mutableListOf<PartETag>().apply {
        futuresPartETags.mapTo(this) { it.get() }
      }

      // Complete the multipart upload
      s3Client.completeMultipartUpload(
        CompleteMultipartUploadRequest(
          bucket, key, uploadId, partETags))
    } catch (e: ExecutionException) {
      e.printStackTrace()
      abortUpload()
      throw RuntimeException(e)
    } catch (e: InterruptedException) {
      e.printStackTrace()
      abortUpload()
      throw RuntimeException(e)
    } catch (e: AmazonS3Exception) {
      e.printStackTrace()
      abortUpload()
      throw RuntimeException(e)
    } finally {
      shutdownAndAwaitTermination()
    }
  }

  private fun abortUpload() {
    log.info("Aborting multipart upload: {} {}, upload ID {}", bucket, key, uploadId)
    s3Client.abortMultipartUpload(AbortMultipartUploadRequest(bucket, key, uploadId))
  }

  /**
   * This method is used to shutdown executor service
   */
  private fun shutdownAndAwaitTermination() {
    log.debug("executor service await and shutdown")
    executorService.shutdown()
    try {
      executorService.awaitTermination(AWAIT_TIME, TimeUnit.SECONDS)
    } catch (e: InterruptedException) {
      log.debug("Interrupted while awaiting ThreadPoolExecutor to shutdown")
    }
    executorService.shutdownNow()
  }

  private fun submitTaskToExecutorService(callable: Callable<PartETag>) {
    // we are submitting each part in executor service and it does not matter which part gets upload first
    // because in each part we have assigned PartNumber from "uploadPartId.incrementAndGet()"
    // and S3 will accumulate file by using PartNumber order after CompleteMultipartUploadRequest
    executorService.submit(callable).let {
      futuresPartETags.add(it)
    }
  }

  companion object {
    private const val AWAIT_TIME = 2L // in seconds
    private const val DEFAULT_THREAD_COUNT = 4
    private val EMPTY_OBJECT_TAGGING = ObjectTagging(emptyList())
  }
}