package tech.functionalflame

import java.io.*
import kotlin.concurrent.thread

/**
 * Code adapted from Pramesh Bhattarai
 *
 * Chunks an [InputStream] into a series of [ByteArrayInputStream]s, allowing for special handling of the final chunk.
 */
class StreamChunker(private val chunkBytes: Int, private val input: InputStream) {
  private val output = ByteArrayOutputStream()

  /**
   * Eagerly consume chunks from the input stream, applying [onChunk] to each as its own stream.
   */
  fun drain(onChunk: (ByteArrayInputStream) -> Unit): StreamRemainingChunk {
    val buffer = ByteArray(chunkBytes)
    var bytesRead: Int
    var bytesAdded = 0

    while (input.read(buffer, 0, buffer.size).also { bytesRead = it } != -1) {
      output.write(buffer, 0, bytesRead)
      if (bytesAdded < chunkBytes) {
        bytesAdded += bytesRead
        continue
      }
      onChunk(ByteArrayInputStream(output.toByteArray()))
      output.reset()
      bytesAdded = 0
    }

    return StreamRemainingChunk()
  }

  inner class StreamRemainingChunk {
    /**
     * Apply [onChunk] to the remaining partial chunk as its own stream.
     */
    fun finally(onChunk: (ByteArrayInputStream) -> Unit) =
      onChunk(ByteArrayInputStream(output.toByteArray()))
  }
}

/**
 * Primitive reimplementation of Java 9+ `InputStream.transferTo`:
 * Pumps the contents of an [InputStream] into an [OutputStream].
 */
@Throws(IOException::class)
fun InputStream.pipe(output: OutputStream) {
  val buffer = ByteArray(8192)
  while (true) {
    val n = read(buffer)
    if (n < 0) break
    output.write(buffer, 0, n)
  }
}

/**
 * Create a piped stream, accepting inputs through the [OutputStream] passed to [doOutput],
 * and emitting outputs through the [InputStream] passed to [doInput].
 */
fun threadPipe(doOutput: (OutputStream) -> Unit, doInput: (InputStream) -> Unit) {
  val output = PipedOutputStream()
  val inThread = thread(start = true) { doOutput(output) }
  PipedInputStream(output).use(doInput)
  inThread.join()
}
